[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                 |
| ---------- | ---------------      |
| 2021-08-04 | Marcel (Create)      |
| 2021-08-18 | Rayana (Ops, Plan & Manage) |
| 2021-09-01 | Jacki                |
| 2021-09-15 | Justin               |
| 2021-09-29 | Taurie               |
| 2021-10-13 | APAC/Europe          |
| 2021-10-27 | Marcel               |
| 2021-11-10 | Rayana               |
| 2021-11-24 | TBD (Plan/Manage)    |
| 2021-12-08 | Jacki                |
| 2021-12-22 | Justin               |
