title: The benefits of GitOps workflows
seo_title: The benefits of GitOps workflows
description: Discover the benefits of GitOps workflows
header_body: GitOps is an operational framework that takes DevOps best practices
  and applies them to infrastructure automation. When teams use a GitOps
  workflow, they experience benefits across the development lifecycle.
canonical_path: /topics/gitops/gitops-best-practices/
file_name: gitops-best-practices
parent_topic: gitops
twitter_image: /images/opengraph/iac-gitops.png
related_content:
  - url: https://about.gitlab.com/topics/gitops/
    title: What is GitOps?
body: >-
  ## A Git version control system enhances security and compliance


  A simplified toolchain reduces attack surfaces, since teams use a single platform for infrastructure management. If an attack does occur, teams can revert to a desired state using the version control system. As a result, GitOps reduces downtime and outages, while enabling teams to continue development in an uncompromised environment.


  Teams that must follow strict compliance often experience decreased [collaboration](/blog/2020/11/23/collaboration-communication-best-practices/) in heavily regulated contexts, where policy often limits the number of people who can enact changes to a production environment. With GitOps, however, anyone can propose a change via a [merge request](/blog/2021/03/18/iteration-and-code-review/), which widens the scope of collaboration while limiting the number of people with the ability to merge to the `production` branch. 


  When teams adopt a GitOps workflow, they experience greater access control, because changes are automated using CI/CD tooling, eliminating the need to provide [access](/blog/2020/02/20/protecting-manual-jobs/) credentials to all infrastructure components. GitOps empowers everyone to contribute, but greater collaboration accompanies a need to maintain a running history of all changes. GitOps ensures that all commits on the `main` [branch](/blog/2021/03/10/new-git-default-branch-name/) act as a change log for auditing.


  ## Established best practices enhance collaboration and productivity


  GitOps incorporates software development best practices for infrastructure as code, Git [workflows](/blog/2020/04/07/15-git-tips-improve-workflow/), and CI/CD pipelines. Operations teams already have these pre-existing skills, knowledge, and toolchain requirements, so the decision to adopt GitOps won’t result in a significant learning curve. GitOps workflows simplify processes to enhance visibility, create a single source of truth, and maintain a lean set of tools.


   A GitOps workflow offers visibility and enhances collaboration, since teams use a Git version control system and merge requests as the mechanism for every infrastructure change. Every update goes through the same review and approval process, and teams can collaborate by sharing ideas, reviewing code, and offering feedback. 


  ## Automation improves the developer experience and reduces cost


  With CI/CD tooling and continuous deployment, productivity increases, because teams benefit from automation and can focus on development rather than investing their efforts on tedious, manual tasks. GitOps workflows improve the developer experience since team members can use whichever language and tools they’d like before pushing updates to Git. There is a low barrier to entry, empowering anyone - from new hires to tenured team members - to become productive quickly and easily. Infrastructure automation improves productivity and reduces downtime, while facilitating better management of cloud resources, which can also decrease [costs](/blog/2020/10/27/how-we-optimized-our-infrastructure-spend-at-gitlab/). Automating infrastructure definition and testing removes manual tasks and rework, while reducing downtimes due to built-in revert and rollback capabilities.


  ## Continuous integrations leads to faster development and deployment


  Teams have an easier time pushing a [minimum viable change](/blog/2020/11/09/lessons-in-iteration-from-new-infrastructure-team/), since GitOps enables faster and more frequent deployments. Using GitOps best practices, teams can ship several times a day and revert changes if there is a problem. High velocity deployments lead to more rapid releases, helping teams deliver business and customer value. With continuous integration, teams are more agile and can quickly respond to customer needs. 


  ## Git workflows increase stability and reliability


  Infrastructure is codified and repeatable, reducing human [error](/blog/2020/01/23/iteration-on-error-tracking/). Merge requests facilitate code reviews and collaboration, and they also help teams identify and correct errors before they make it to production. There is also less risk, since all changes to infrastructure are tracked through [merge requests](/blog/2020/12/14/merge-trains-explained/), and changes can be rolled back to a previous state if an iteration doesn’t work out well. Git workflows reduce recovery time by enabling rollbacks to a more stable state and offering [distributed](https://git-scm.com/about/distributed) backup copies in the event of a serious outage. GitOps empowers teams to iterate faster to ship new features without the fear of causing an unstable environment.
benefits_title: ""
benefits_description: ""
benefits: []
resources_title: Ready to learn more about GitOps?
resources:
  - url: https://about.gitlab.com/solutions/gitops/
    title: Discover how GitLab streamlines GitOps workflos
    type: Articles
  - url: https://about.gitlab.com/why/gitops-infrastructure-automation/
    type: Webcast
    title: Learn about the future of GitOps from tech leaders
  - title: Download the beginner’s guide to GitOps
    url: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
    type: Books
